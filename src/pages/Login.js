// Identify the component that will be used for this page.

import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
import Hero from './../components/Banner';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';

// we will declare state for our form components for us to be able to access and manage the values in each of the form elements.

const data = {
	title: 'Welcome to Login',
	content: 'Sign in your account below'
}
// Create a fundtion that will describe the structure of the page.
export default function Login() {

	const {user, setUser} = useContext(UserContext);

	// declare an 'initial'/default state for our form elements.
	// Bind/Lock the form elements to the desired states
	// Assign the states to their respective components
	// Syntax: const/let [getter, setter] = useState()
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	// Validation to run on the email
	// format: @, dns
	// Search a character withing a string
	let addressSign = email.search('@')
	// If the search() finds No match inside the string it will return -1.
	// to pass down multiple values on the query, contain it inside an array. includes()
	let dns = email.search('.com')

	// Declare a state for the login button
	const [isActive, setIsActive] = useState(false);
	// Apply a conditional rendering to the button component for its current

	const [isValid, setIsValid] = useState(false);

	// Create a side effect that will make our page reactive.
	useEffect(() => {
		// Create a logic/condition that will evaluate the format of the email.
		if (dns !== -1 && addressSign !== -1) {
			setIsValid(true);
			if (password !== '') {
				setIsActive(true);
			} else {
				setIsActive(false);
			}
		} else {
			setIsValid(false);
			setIsActive(false);
		}
	},[email, password, addressSign, dns])

	// Create a simulation that will help us identify the workflow of the login page.
	// Create a function that will run the request for user authentication to product an access token.
	const loginUser = async (event) => {
			event.preventDefault();
			
			// Send a request to verify if the user's identity is true.
			fetch('https://glacial-everglades-19835.herokuapp.com/users/login', {
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
    				email : email,
    				password : password
				})
			}).then(res => res.json())
			.then(jsonData => {
				// console.log(jsonData);
				let token = jsonData.accessToken;
				// console.log(token);

				// Create a control structure to give a proper response to the user 
				if (typeof token !== 'undefined') {
					// Prompt a message to the user

					// The produced token, save it on the browser storage. (storage area)
					// To save an item in the localStorage object of the browser use setItem(key, value)
					localStorage.setItem('accessToken' ,token);

					fetch('https://glacial-everglades-19835.herokuapp.com/users/details', {
					  headers: {
					    Authorization: `Bearer ${token}`
					  }
					}).then(res => res.json()).then(convertedData => {  
					  if (typeof convertedData._id !== "undefined") {
					      setUSer({
					        id: convertedData._id,
					        isAdmin: convertedData.isAdmin
					      });					      
							Swal.fire({
								icon: 'success',
								title: 'Login Successful',
								text: 'Welcome'
							})	
					  } else {					      
					      setUSer({
					        id: null,
					        isAdmin: null
					      });          
					  }
					});


				} else {
					Swal.fire({
						icon: 'error',
						title: 'Check your Credentials',
						text: 'Contact Admin if problem persist'
					})	
				}
			})				
	};

	return(
		user.id ?
			<Navigate to="/courses" replace={true} />
		:
		<>
			<Hero bannerData={data} />
			<Container>
				<h1 className="text-center">Login Form</h1>
				<Form onSubmit={e => loginUser(e)}>
					{/*Email Address Field*/}
					<Form.Group>
						<Form.Label>Email:</Form.Label>
						<Form.Control 
						type="email" 
						placeholder="Enter Email Here" 
						required 
						value={email} 
						onChange={event => {setEmail(event.target.value)}} />
						{
							isValid ?
								<h6 className="text-success">Email is Valid</h6>
							:
							<h6 className="text-mute">Email is Invalid</h6>
						}
						
						
					</Form.Group>

					{/*Password Field*/}
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form.Control 
						type="password" 
						placeholder="Enter Password Here" 
						required 
						value={password} 
						onChange={e => {setPassword(e.target.value)}} />
					</Form.Group>
					{
						isActive ?
						<Button 
						className="btn-block"
						variant="success" 
						type="submit">
						Login
						</Button>
						:
						<Button 
						className="btn-block" 
						variant="secondary" 
						disabled>Login</Button>
					}


				</Form>
			</Container>
		</>
	);
};