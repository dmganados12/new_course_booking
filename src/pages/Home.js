// Acquire all the component that will make up hero section, highlights
import Banner from './../components/Banner'
import Highlights from './../components/Highlights'

// Identify first how yo want the landing to page to be structured

// When wrapping multiple adjacent JSX elements
	// => div element
	// => React Fragment - Groups elements
		// - invisible div
		// - Container
		// Syntax: Long version <React.Fragment> || <Fragment>
		// short <> </>

// Let's create a data object that will describe the content of the hero section
const details ={
	title: 'Welcome to the Home Page',
	content: 'Opportunities for everyone, everywhere'
};

export default function Home() {
	return(
		<div>
			<Banner bannerData={details}/>
			<Highlights />
		</div>
	);
}
