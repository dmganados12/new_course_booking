// This will serve as page whenever the client would want to display or select a single course/item from the catalog
import {useState, useEffect} from 'react'
import Hero from './../components/Banner';
// Grid System: Card, Button
import {Row, Col, Card, Button, Container} from 'react-bootstrap';

// Declare a state for the course details. We need to use the correct 'hook'. 


// routing component
import {Link, useParams} from 'react-router-dom';

// import sweetalert
import Swal from 'sweetalert2';

const data = {
	title: 'Welcome to B156 Booking-App',
	content: 'Check out our school campus'
}

export default function CourseView(){

	// State of our course details
	const [courseInfo, setCourseInfo] = useState({
		name: null,
		description: null,
		price: null
	});

	// Retrieve the data from the url to extract the course id that will be used as a reference to determine which course will be displayed by the page. Acquire a 'hook' which will allow us to manage/access the date in the browser URL.

	// Using the parameter Hook(useParams) -> this will be provided by 'react-router-dom'

	// (useState, useEffect, useParams)

	// The useParams will return an object that will conain the path variables stored in the URL.
	console.log(useParams()); //observe, it should contain an object that stores the id of the targeted course.

	// extract the valued from the path variables by structureing the object
	const {id} = useParams();
	console.log(id);

	// Create a 'side effect' which will send a request to our backend API for course-booking. Use the proper 'hook' (effect hook);
	useEffect(() => {

		// Send a requiest to our API to retrieve information about the course.
		// Syntax: fetch('<URL address>', '<OPTION>')
		// Upon sending this req to the API a promise will be initialized.
		fetch(`https://glacial-everglades-19835.herokuapp.com/courses/${id}`).then(res => res.json()).then(convertedData => {/*console.log(convertedData);*/
			// The data that we retrieved from the db, we need to contain.
			// Call the setter to change the state of the course info for this page
			setCourseInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price
			})
		});
	},[id]);

	const enroll = () => {
		return(
			Swal.fire({
				icon: 'success',
				title: 'Enrolled Successfully',
				text: 'Thank you for enrolling to this course'
			})			
		);
	};

	return(
		<>
			<Hero bannerData={data} />
			<Row>
				<Col>
					<Container>
					<Card className="text-center">
						<Card.Body>
							{/*Course Name*/}
							<Card.Title>
								<h1> {courseInfo.name} </h1>
							</Card.Title>
							{/*Course Description*/}
							<Card.Subtitle>
								<h6 className="my-4"> Description: </h6>
							</Card.Subtitle>
							<Card.Text>
								{courseInfo.description}
							</Card.Text>
							{/*Course Price*/}
							<Card.Subtitle>
								<h6 className="my-4"> Price: </h6>
							</Card.Subtitle>
							<Card.Text>
								PHP {courseInfo.price}
							</Card.Text>
						</Card.Body>
						<Button variant="warning" className="btn-block" onClick ={enroll}>
							Enroll
						</Button>

						<Link className="btn btn-success btn-block mb-5" to="/login">
							Login to Enroll
						</Link>
					</Card>
					</Container>
				</Col>
			</Row>
		</>
	);
};