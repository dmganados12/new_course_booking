import Hero from './../components/Banner';
import {Container, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

const data = {
	title: 'Add New Course',
	content: 'Please fill in all fields to create a new course.'
}
export default function AddCourse() {

	const newCourse = (eventSubmit) => {
		eventSubmit.preventDefault();
		
		return(
			Swal.fire({
				icon: "success",
				title: "New Course Creation Successful"
			})

		);
	};

	return(
		<div>
			<Hero bannerData={data} />

			<Container>
				<h1 className="text-center">Create Course Form</h1>
				<Form onSubmit={e => newCourse(e)}>
					{/*Course Name Field*/}
					<Form.Group>
						<Form.Label>Course Name</Form.Label>
						<Form.Control type="text" placeholder="Ex: English 101" required />
					</Form.Group>

					{/*Course Description Field*/}
					<Form.Group>
						<Form.Label>Description</Form.Label>
						<Form.Control type="text" placeholder="Ex. Concentrates primarily on expository and argumentative writing." required />
					</Form.Group>

					{/*Course Price Field*/}
					<Form.Group>
						<Form.Label>Price:</Form.Label>
						<Form.Control type="number" placeholder="Ex. 500.00" required />
					</Form.Group>

					<Button className="btn-block" type="submit">Create this Course</Button>

				</Form>
			</Container>
		</div>
	);
};