import {Card, Row, Col, Container} from 'react-bootstrap';

// Element with routing capability
import {Link} from 'react-router-dom';
// we are goind to use this Link component to not only redirect the user the course page but to carry data needed to identify which course display

export default function CourseCard({courseProp}) {
	return(						
		<Col xs={12} md={4}>
			<Card className="p-4">					
				<Card.Body>
					<Card.Title>
						{courseProp.name}
					</Card.Title>
					<Card.Text>
						{courseProp.description}
					</Card.Text>
					<Card.Text>
						Price: {courseProp.price}
					</Card.Text>
						<Link to={`view/${courseProp._id}`} className="btn btn-primary">
							View Course
						</Link>							
				</Card.Body>					
			</Card>
		</Col>		
	)
}
